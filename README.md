# TERMINET IIoT-DI Middleware

This repo provides the API of the I-IoT-DI Middleware (MDW) of TERMINET, that aims to facilitate the interactions between the FINoT platform (I-IoT-DI Core) that provides the Inventory services and the rest of TERMINET entities besides enabling other TERMINET entities to programmatically access the provided inventory services in a seamless and unified way.

## Authors and acknowledgment
Future Intelligence TERMINET Development team.

## License
EUPL v1.2

