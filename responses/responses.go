package responses

import "terminet-iot-di-mdw/models"

type CommonResponseAttributes struct {
	Status  int    `json:"status"`
	Message string `json:"message,omitempty"`
}

type InventoryItemResponse struct {
	CommonResponseAttributes
	Data models.InventoryObject `json:"data,omitempty"`
}

type InventoryListResponse struct {
	CommonResponseAttributes
	Data []models.InventoryObject `json:"data,omitempty"`
}

type ErrorResponse struct {
	CommonResponseAttributes
	Data map[string]interface{} `json:"errorData,omitempty"`
}
