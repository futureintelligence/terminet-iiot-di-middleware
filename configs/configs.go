package configs

const (
	FINOTTERMINETAUT_API = "https://**************" // replaced for security reasons, please contact FINT or TERMINET consortium for getting the relevant endpoint
	FINOTTERMINET_API    = "https://*********/"     // replaced for security reasons, please contact FINT or TERMINET consortium for getting the relevant endpoint
	TENANT_CREDS_FILE    = "path to creds file"     // please consider another mechanism for retrieving your FINoT credentials
)
