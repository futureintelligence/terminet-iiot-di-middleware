package controllers

import (
	"net/http"
	"terminet-iot-di-mdw/models"
	"terminet-iot-di-mdw/responses"

	"github.com/gin-gonic/gin"
	"github.com/rs/zerolog/log"
)

// set logger for package
var logger = models.SetLogger()

// Create Inventory object
// @Summary of Create Inventory Object
// @Schemes
// @Description Use to create an Inventory Object
// @Param	body		body 	models.InventoryObject	true		"Inventory Object Data"
// @Tags Inventory Operations
// @Accept json
// @Produce json
// @Success 201 {object} responses.InventoryListResponse
// @Failure 401 {object} responses.ErrorResponse
// @Failure 400 {object} responses.ErrorResponse
// @Router /objects [post]
func CreateInventoryObjects(c *gin.Context) {

	var invObjs *models.InventoryObject
	invObjs = new(models.InventoryObject)

	// Call BindJSON to bind the received JSON to
	// newAlbum.
	if err := c.BindJSON(&invObjs); err != nil {
		log.Err(err).Msg(err.Error())

		return
	}

	err := invObjs.CreateInventoryObject()

	if err != nil {
		log.Err(err).Msg(err.Error())
		c.JSON(http.StatusNotFound, responses.ErrorResponse{CommonResponseAttributes: responses.CommonResponseAttributes{Status: http.StatusNotFound, Message: "error:" + err.Error()}})
		return
	}
	x := responses.CommonResponseAttributes{Status: http.StatusOK, Message: "Success"}
	c.JSON(http.StatusOK, responses.InventoryItemResponse{x, *invObjs})

}

// Update Inventory object
// @Summary of Update Inventory Object
// @Schemes
// @Description Use to update an Inventory Object
// @Param objectid path string true "the id of the Inventory Object you want to get"
// @Param	body		body 	models.InventoryObject	true		"Inventory Object Data"
// @Tags Inventory Operations
// @Accept json
// @Produce json
// @Success 200 {object} responses.InventoryListResponse
// @Failure 401 {object} responses.ErrorResponse
// @Failure 400 {object} responses.ErrorResponse
// @Router /objects/{objectid} [patch]
func UpdateInventoryObjects(c *gin.Context) {

	var invObjs *models.InventoryObject
	invObjs = new(models.InventoryObject)

	// Call BindJSON to bind the received JSON to
	// newAlbum.
	if err := c.BindJSON(&invObjs); err != nil {
		log.Err(err).Msg(err.Error())

		return
	}

	objectId := c.Param("objectid")

	invObjs.ID = objectId
	err := invObjs.UpdateInventoryObject()

	if err != nil {
		log.Err(err).Msg(err.Error())
		c.JSON(http.StatusNotFound, responses.ErrorResponse{CommonResponseAttributes: responses.CommonResponseAttributes{Status: http.StatusNotFound, Message: "error:" + err.Error()}})
		return
	}
	x := responses.CommonResponseAttributes{Status: http.StatusOK, Message: "Success"}
	c.JSON(http.StatusOK, responses.InventoryItemResponse{x, *invObjs})

}

// Get Inventory objects
// @Summary of Get Inventory Objects
// @Schemes
// @Description Use to get the list of the Inventory Objects
// @Param type query string false "the type of the objects you want to get"
// @Tags Inventory Operations
// @Accept json
// @Produce json
// @Success 200 {object} responses.InventoryListResponse
// @Failure 401 {object} responses.ErrorResponse
// @Failure 400 {object} responses.ErrorResponse
// @Router /objects [get]
func GetInventoryObjects(c *gin.Context) {

	var invObjs *models.InventoryObjects
	invObjs = new(models.InventoryObjects)
	res, err := invObjs.GetInventoryObjects()

	if err != nil {
		log.Err(err).Msg(err.Error())
		c.JSON(http.StatusNotFound, responses.ErrorResponse{CommonResponseAttributes: responses.CommonResponseAttributes{Status: http.StatusNotFound, Message: "error:" + err.Error()}})
		return
	}
	x := responses.CommonResponseAttributes{Status: http.StatusOK, Message: "Success"}
	c.JSON(http.StatusOK, responses.InventoryListResponse{x, res})

}

// Get Inventory object by id
// @Summary of Get Inventory Object by id
// @Summary Get Inventory Object by id
// @Schemes
// @Description Use to get the Inventory Object matching the given id
// @Param objectid path string true "the id of the Inventory Object you want to get"
// @Tags Inventory Operations
// @Accept json
// @Produce json
// @Success 200 {object} responses.InventoryItemResponse
// @Failure 401 {object} responses.ErrorResponse
// @Failure 400 {object} responses.ErrorResponse
// @Router /objects/{objectid} [get]
func GetInventoryObjectById(c *gin.Context) {
	objectId := c.Param("objectid")

	// path, err = os.Executable()
	// if err != nil {
	// 	fmt.Println(err)
	// }
	// fmt.Println(path)
	var invObj *models.InventoryObject

	invObj = new(models.InventoryObject)
	invObj.ID = objectId
	err := invObj.GetInventoryObject()

	if err != nil {
		log.Err(err).Msg(err.Error())
		c.JSON(http.StatusNotFound, responses.ErrorResponse{CommonResponseAttributes: responses.CommonResponseAttributes{Status: http.StatusNotFound, Message: "error:" + err.Error()}})
		return
	}
	x := responses.CommonResponseAttributes{Status: http.StatusOK, Message: "Success"}
	c.JSON(http.StatusOK, responses.InventoryItemResponse{x, *invObj})

}

// Delete Inventory object by id
// @Summary of Delete Inventory Object
// @Summary Delete Inventory Object by id
// @Schemes
// @Description Use to delete the Inventory Object matching the given id
// @Param objectid path string true "the id of the Inventory Object you want to delete"
// @Tags Inventory Operations
// @Accept json
// @Produce json
// @Success 204 {string} No content
// @Failure 401 {object} responses.ErrorResponse
// @Failure 400 {object} responses.ErrorResponse
// @Router /objects/{objectid} [delete]
func DeleteInventoryObjectById(c *gin.Context) {
	objectId := c.Param("objectid")

	// path, err = os.Executable()
	// if err != nil {
	// 	fmt.Println(err)
	// }
	// fmt.Println(path)
	var invObj *models.InventoryObject

	invObj = new(models.InventoryObject)
	invObj.ID = objectId
	err := invObj.DeleteInventoryObject()

	if err != nil {
		log.Err(err).Msg(err.Error())
		c.JSON(http.StatusNotFound, responses.ErrorResponse{CommonResponseAttributes: responses.CommonResponseAttributes{Status: http.StatusNotFound, Message: "error:" + err.Error()}})
		return
	}
	x := responses.CommonResponseAttributes{Status: http.StatusNoContent, Message: "Success"}
	c.JSON(http.StatusNoContent, responses.InventoryItemResponse{x, models.InventoryObject{}})

}
