basePath: /v1
definitions:
  models.InventoryObject:
    properties:
      attributes:
        additionalProperties: true
        type: object
      dateCreated:
        type: string
      devId:
        type: string
      id:
        type: string
      location:
        $ref: '#/definitions/models.ObjectLocation'
      name:
        type: string
      owner:
        type: string
      status:
        type: string
      type:
        type: string
      useCase:
        type: string
    type: object
  models.ObjectLocation:
    properties:
      coordinates:
        items:
          type: number
        type: array
      type:
        type: string
    type: object
  responses.ErrorResponse:
    properties:
      errorData:
        additionalProperties: true
        type: object
      message:
        type: string
      status:
        type: integer
    type: object
  responses.InventoryItemResponse:
    properties:
      data:
        $ref: '#/definitions/models.InventoryObject'
      message:
        type: string
      status:
        type: integer
    type: object
  responses.InventoryListResponse:
    properties:
      data:
        items:
          $ref: '#/definitions/models.InventoryObject'
        type: array
      message:
        type: string
      status:
        type: integer
    type: object
host: <IP of container providing mdw>:9997
info:
  contact:
    email: asideris@f-in.eu
    name: Argiris Sideris
  description: This is the API documentation for the TERMINET  Inventory  of intelligent
    IoT devices (I-IoT-DI) Middleware.
  license:
    name: EUPL 1.2
    url: https://commission.europa.eu/content/european-union-public-licence_en
  title: TERMINET I-IoT-DI MDW API
  version: "1.0"
paths:
  /objects:
    get:
      consumes:
      - application/json
      description: Use to get the list of the Inventory Objects
      parameters:
      - description: the type of the objects you want to get
        in: query
        name: type
        type: string
      produces:
      - application/json
      responses:
        "200":
          description: OK
          schema:
            $ref: '#/definitions/responses.InventoryListResponse'
        "400":
          description: Bad Request
          schema:
            $ref: '#/definitions/responses.ErrorResponse'
        "401":
          description: Unauthorized
          schema:
            $ref: '#/definitions/responses.ErrorResponse'
      summary: of Get Inventory Objects
      tags:
      - Inventory Operations
    post:
      consumes:
      - application/json
      description: Use to create an Inventory Object
      parameters:
      - description: Inventory Object Data
        in: body
        name: body
        required: true
        schema:
          $ref: '#/definitions/models.InventoryObject'
      produces:
      - application/json
      responses:
        "201":
          description: Created
          schema:
            $ref: '#/definitions/responses.InventoryListResponse'
        "400":
          description: Bad Request
          schema:
            $ref: '#/definitions/responses.ErrorResponse'
        "401":
          description: Unauthorized
          schema:
            $ref: '#/definitions/responses.ErrorResponse'
      summary: of Create Inventory Object
      tags:
      - Inventory Operations
  /objects/{objectid}:
    delete:
      consumes:
      - application/json
      description: Use to delete the Inventory Object matching the given id
      parameters:
      - description: the id of the Inventory Object you want to delete
        in: path
        name: objectid
        required: true
        type: string
      produces:
      - application/json
      responses:
        "204":
          description: No Content
          schema:
            type: string
        "400":
          description: Bad Request
          schema:
            $ref: '#/definitions/responses.ErrorResponse'
        "401":
          description: Unauthorized
          schema:
            $ref: '#/definitions/responses.ErrorResponse'
      summary: Delete Inventory Object by id
      tags:
      - Inventory Operations
    get:
      consumes:
      - application/json
      description: Use to get the Inventory Object matching the given id
      parameters:
      - description: the id of the Inventory Object you want to get
        in: path
        name: objectid
        required: true
        type: string
      produces:
      - application/json
      responses:
        "200":
          description: OK
          schema:
            $ref: '#/definitions/responses.InventoryItemResponse'
        "400":
          description: Bad Request
          schema:
            $ref: '#/definitions/responses.ErrorResponse'
        "401":
          description: Unauthorized
          schema:
            $ref: '#/definitions/responses.ErrorResponse'
      summary: Get Inventory Object by id
      tags:
      - Inventory Operations
    patch:
      consumes:
      - application/json
      description: Use to update an Inventory Object
      parameters:
      - description: the id of the Inventory Object you want to get
        in: path
        name: objectid
        required: true
        type: string
      - description: Inventory Object Data
        in: body
        name: body
        required: true
        schema:
          $ref: '#/definitions/models.InventoryObject'
      produces:
      - application/json
      responses:
        "200":
          description: OK
          schema:
            $ref: '#/definitions/responses.InventoryListResponse'
        "400":
          description: Bad Request
          schema:
            $ref: '#/definitions/responses.ErrorResponse'
        "401":
          description: Unauthorized
          schema:
            $ref: '#/definitions/responses.ErrorResponse'
      summary: of Update Inventory Object
      tags:
      - Inventory Operations
swagger: "2.0"
