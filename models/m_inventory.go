package models

import (
	"encoding/json"
	"errors"
	"fmt"
	"reflect"
)

type InventoryObject struct {
	ID          string                 `json:"id,omitempty"`
	DevID       string                 `json:"devId,omitempty"`
	Type        string                 `json:"type,omitempty"`
	Name        string                 `json:"name,omitempty"`
	Owner       string                 `json:"owner,omitempty"`
	UseCase     string                 `json:"useCase,omitempty"`
	Status      string                 `json:"status,omitempty"`
	DateCreated string                 `json:"dateCreated,omitempty"`
	Location    ObjectLocation         `json:"location,omitempty"`
	Attributes  map[string]interface{} `json:"attributes,omitempty"`
}

type ResponseInventoryObject struct {
	ID          string                 `json:"id,omitempty"`
	DevID       string                 `json:"devId,omitempty"`
	Type        string                 `json:"type,omitempty"`
	Name        string                 `json:"name,omitempty"`
	Owner       string                 `json:"owner,omitempty"`
	UseCase     string                 `json:"useCase,omitempty"`
	Status      string                 `json:"status,omitempty"`
	DateCreated string                 `json:"dateCreated,omitempty"`
	Location    ObjectLocation         `json:"location,omitempty"`
	Attributes  map[string]interface{} `json:"attributes,omitempty"`
}

type InventoryObjects []InventoryObject

type ObjectLocation struct {
	Type        string    `json:"type,omitempty"`
	Coordinates []float64 `json:"coordinates,omitempty"`
}

func (o *InventoryObject) CreateInventoryObject() (err error) {

	var token string
	token, err = getToken()
	if err != nil {
		logger.Error().Msg(err.Error())
		return err

	}
	res, err := o.mapInventory2FINoT()

	xr, _ := json.Marshal(res)

	fmt.Println(string(xr))

	if err != nil {

		logger.Error().Msg(err.Error())
		return err

	}

	resp, err := res.uploadDataToFinot(token)
	if resp.Status != 201 {

		msg, _ := json.Marshal(res)

		logger.Error().Msg(string(msg))

		return errors.New(string(msg))
	}

	return err
}

func (o *InventoryObject) UpdateInventoryObject() (err error) {

	var token string
	token, err = getToken()
	if err != nil {
		logger.Error().Msg(err.Error())
		return err

	}
	res, err := o.mapUpdateInventory2FINoT()

	xr, _ := json.Marshal(res)

	fmt.Println(string(xr))

	if err != nil {

		logger.Error().Msg(err.Error())
		return err

	}

	var bytes []byte
	bytes, err = json.Marshal(res)
	resp, err := uploadUpdatedDataToFinot(o.ID, token, bytes)

	fmt.Println(resp.Status)
	if resp.Status != 200 {

		msg, _ := json.Marshal(res)

		logger.Error().Msg(string(msg))

		return errors.New(string(msg))
	}

	return err
}

func (o *InventoryObjects) GetInventoryObjects() (objects []InventoryObject, err error) {
	var token string
	token, err = getToken()
	if err != nil {
		logger.Error().Msg(err.Error())
		return objects, err

	}
	res, err := GetDataFromFinot("", token)
	fmt.Println(res)

	if err != nil {
		// msg, _ := json.Marshal(res.(FINoTResponseDataModel))

		logger.Error().Msg(err.Error())
		return objects, err

	}

	if res.(map[string]interface{})["status"] != float64(200) {

		msg, _ := json.Marshal(res)

		logger.Error().Msg(string(msg))

		return objects, errors.New(string(msg))
	}

	for k, v := range res.(map[string]interface{})["result"].(map[string]interface{})["object"].([]interface{}) {
		fmt.Println(k, "---", v)
		var invObj *InventoryObject
		invObj = new(InventoryObject)
		var xa map[string]interface{}
		xa = make(map[string]interface{})
		xa["object"] = v.(map[string]interface{})
		err = invObj.mapFINoT2Inventory(xa)
		if err != nil {
			logger.Error().Msg(err.Error())

		}
		objects = append(objects, *invObj)

	}

	return objects, err
}

func (o *InventoryObject) GetInventoryObject() (err error) {
	var token string
	token, err = getToken()
	if err != nil {
		logger.Error().Msg(err.Error())
		return err

	}
	res, err := GetDataFromFinot(o.ID, token)

	if err != nil {
		// msg, _ := json.Marshal(res.(FINoTResponseDataModel))

		logger.Error().Msg(err.Error())
		return err

	}

	mpRes, err := struct2Map(res)

	if err != nil {
		// msg, _ := json.Marshal(res.(FINoTResponseDataModel))

		logger.Error().Msg(err.Error())
		return err

	}

	if mpRes["status"] != float64(200) {

		msg, _ := json.Marshal(res)

		logger.Error().Msg(string(msg))

		return errors.New(string(msg))
	}
	err = o.mapFINoT2Inventory(mpRes["result"].(map[string]interface{}))

	return err
}

func (o *InventoryObject) DeleteInventoryObject() (err error) {
	var token string
	token, err = getToken()
	if err != nil {
		logger.Error().Msg(err.Error())
		return err

	}
	_, err = DeleteDataFromFinot(o.ID, token)

	if err != nil {
		// msg, _ := json.Marshal(res.(FINoTResponseDataModel))

		logger.Error().Msg(err.Error())

	}

	return err
}

func getToken() (jwt string, err error) {

	var token FinotToken
	// var tokenPreamble string = "JWT "
	err = token.GetFinotToken()
	if err != nil {
		logger.Error().Msg(err.Error())
		return token.Token, err

	}

	return token.Token, err
}

func struct2Map(data interface{}) (objectMap map[string]interface{}, err error) {
	jsonStr, err := json.Marshal(data)
	err = json.Unmarshal([]byte(jsonStr), &objectMap)
	return objectMap, err
}

func (o *InventoryObject) mapFINoT2Inventory(data map[string]interface{}) (err error) {
	o.Attributes = make(map[string]interface{})
	for k, v := range data["object"].(map[string]interface{}) {

		fmt.Println(k, "---", v)

		switch k {
		case "TimeInstant":
		case "id":
			o.ID = v.(string)
		case "refSubscription":

		case "type":
			o.Type = v.(string)
		case "dateCreated":
			o.DateCreated = v.(map[string]interface{})["value"].(string)
		case "location":
			o.Location.Type = v.(map[string]interface{})["value"].(map[string]interface{})["type"].(string)
			for k, v := range v.(map[string]interface{})["value"].(map[string]interface{})["coordinates"].([]interface{}) {
				fmt.Println(k, "----", v)
				o.Location.Coordinates = append(o.Location.Coordinates, v.(float64))
			}
		default:
			o.Attributes[k] = v

		}
	}
	return err
}

func (o *InventoryObject) mapInventory2FINoT() (finotData *FINoTDataModel, err error) {
	var attributeTypes = []string{"Text", "geo:json"}

	finotData = new(FINoTDataModel)
	var loc map[string]interface{}
	loc = make(map[string]interface{})

	loc["type"] = "Point"
	loc["coordinates"] = o.Location.Coordinates
	finotData.Type = o.Type
	finotData.StaticAttributes = append(finotData.StaticAttributes, StaticAttribute{Type: attributeTypes[0], Value: o.Name, Name: "name"})
	finotData.StaticAttributes = append(finotData.StaticAttributes, StaticAttribute{Type: attributeTypes[0], Value: o.Owner, Name: "owner"})
	finotData.StaticAttributes = append(finotData.StaticAttributes, StaticAttribute{Type: attributeTypes[0], Value: o.UseCase, Name: "useCase"})
	finotData.StaticAttributes = append(finotData.StaticAttributes, StaticAttribute{Type: attributeTypes[0], Value: o.DevID, Name: "devId"})
	// finotData.Attributes = append(finotData.Attributes, Attribute{Type: attributeTypes[0], Value: o.Status, Name: "status"})
	finotData.Attributes = append(finotData.Attributes, Attribute{Type: attributeTypes[1], Name: "location", Value: loc})

	for k, v := range o.Attributes {

		fmt.Println(k, "---", v)

		finotData.Attributes = append(finotData.Attributes, Attribute{Type: attributeTypes[0], Value: v, Name: k})
	}

	return finotData, err
}

func (o *InventoryObject) mapUpdateInventory2FINoT() (finotAttributes map[string]interface{}, err error) {
	// var attributeTypes = []string{"Text", "geo:json"}
	mappedData, err := struct2Map(o)
	finotAttributes = make(map[string]interface{})

	for k, v := range mappedData {
		if k == "id" || k == "type" || k == "dateCreated" {
			continue
		}
		var key string = k
		var value interface{}
		switch reflect.TypeOf(v).String() {
		case "string":
			value = v.(string)
		case "[]interface {}":

			value = v.([]interface{})
		case "map[string]interface {}":

			value = v.(map[string]interface{})
			// continue

		case "float64":
			value = fmt.Sprintf("%.2f", v.(float64))
		}

		finotAttributes[key] = map[string]interface{}{"value": value}

	}

	return finotAttributes, err
}
