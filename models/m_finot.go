package models

import (
	"bytes"
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	"net/http"
	"terminet-iot-di-mdw/configs"
)

type FINoTDataModel struct {
	Type             string            `json:"type,omitempty"`
	TimeInstant      string            `json:"TimeInstant,omitempty"`
	Attributes       []Attribute       `json:"attributes,omitempty"`
	StaticAttributes []StaticAttribute `json:"static_attributes,omitempty"`
}

type Attribute struct {
	Name string `json:"name,omitempty"`
	Type string `json:"type,omitempty"`

	Value    interface{} `json:"value"`
	Metadata *Metadata   `json:"metadata,omitempty"`
}

type Metadata struct {
	UnitCode    MetadataStruct `json:"unitCode,omitempty"`
	TimeInstant MetadataStruct `json:"TimeInstant,omitempty"`
}

type MetadataStruct struct {
	Type  string `json:"type,omitempty"`
	Value string `json:"value,omitempty"`
}

type StaticAttribute struct {
	Name  string `json:"name,omitempty"`
	Type  string `json:"type,omitempty"`
	Value string `json:"value"`
}

type FINoTUpdateDataModel struct {
	TimeInstant string      `json:"TimeInstant,omitempty"`
	Name        string      `json:"name,omitempty"`
	Type        string      `json:"type,omitempty"`
	Value       interface{} `json:"value"`
	Metadata    Metadata    `json:"metadata,omitempty"`
}

type Location struct {
	Type        string    `json:"type,omitempty"`
	Coordinates []float64 `json:"coordinates,omitempty"`
}

type FINoTResponseHistoricalDataModel struct {
	Status int `json:"status"`
	Result struct {
		Data struct {
			Attributes []struct {
				AttrName string        `json:"attrName"`
				Values   []interface{} `json:"values"`
			} `json:"attributes"`
			EntityID string   `json:"entityId"`
			Index    []string `json:"index"`
		} `json:"data"`
	} `json:"result"`
}

type FINoTResponseDataModel struct {
	Status int64       `json:"status,omitempty"`
	Result FINoTResult `json:"result,omitempty"`
}

type FINoTResult struct {
	Object Object `json:"object,omitempty"`
}

type Object struct {
	ID                string              `json:"id,omitempty"`
	Type              string              `json:"type,omitempty"`
	TimeInstant       TimeInstantResponse `json:"TimeInstant"`
	DynamicAttributes map[string]interface{}
}

type FINoTResponseMeasurement struct {
	Metadata MetadataResponse `json:"metadata"`
	Type     string           `json:"type"`
	Value    interface{}      `json:"value"`
}
type TimeInstantResponse struct {
	Type  string `json:"type"`
	Value string `json:"value"`
}
type IsDynamic struct {
	Type  string `json:"type"`
	Value bool   `json:"value"`
}
type MetadataResponse struct {
	TimeInstant TimeInstantResponse `json:"TimeInstant"`
	IsDynamic   IsDynamic           `json:"isDynamic"`
}

func (r *FINoTDataModel) Marshal() ([]byte, error) {
	return json.Marshal(r)
}

func GetDataFromFinot(id string, token string) (response interface{}, err error) {

	var tokenPreamble string = "JWT "

	url := configs.FINOTTERMINET_API + "inventory/v1/objects/" + id
	// logs.Info(url)
	req, err := http.NewRequest("GET", url, nil)
	req.Header.Set("Content-Type", "application/json")
	req.Header.Set("X-Tenant", "terminet")
	req.Header.Set("Authorization", tokenPreamble+token)
	if err != nil {
		logger.Error().Msg(err.Error())
		return nil, err
	}
	client := &http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		logger.Error().Msg(err.Error())
		return nil, err
	}
	body, _ := ioutil.ReadAll(resp.Body)
	// logs.Info(string(body))
	// var res interface{}
	err = json.Unmarshal(body, &response)
	if err != nil {
		logger.Error().Msg(err.Error())
		return FINoTResponseDataModel{}, err
	}

	// logger.Info().Msg(string(body))
	return response, err

}

func (finot *FINoTDataModel) uploadDataToFinot(token string) (finotResponse FINoTResponseDataModel, err error) {

	var tokenPreamble string = "JWT "
	v, err := finot.Marshal()
	req, err := http.NewRequest("POST", configs.FINOTTERMINET_API+"inventory/v1/objects", bytes.NewBuffer(v))
	req.Header.Set("Content-Type", "application/json")
	req.Header.Set("X-Tenant", "terminet")
	req.Header.Set("Authorization", tokenPreamble+token)

	if err != nil {
		logger.Error().Msg(err.Error())
		return FINoTResponseDataModel{}, err
	}
	client := &http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		logger.Error().Msg(err.Error())
		return FINoTResponseDataModel{}, err
	}
	body, _ := ioutil.ReadAll(resp.Body)
	logger.Info().Msg(string(body))
	// var res interface{}
	err = json.Unmarshal(body, &finotResponse)
	if err != nil {
		logger.Error().Msg(err.Error())
		return FINoTResponseDataModel{}, err
	}

	return finotResponse, nil
}

func DeleteDataFromFinot(id string, token string) (response interface{}, err error) {

	// var token FinotToken
	var tokenPreamble string = "JWT "
	// err = token.GetFinotToken()
	// if err != nil {
	// 	logger.Error().Msg(err.Error())
	// 	return FINoTResponseDataModel{}, err

	// }
	url := configs.FINOTTERMINET_API + "inventory/v1/objects/" + id
	// logs.Info(url)
	req, err := http.NewRequest("DELETE", url, nil)
	req.Header.Set("Content-Type", "application/json")
	req.Header.Set("X-Tenant", "terminet")
	req.Header.Set("Authorization", tokenPreamble+token)
	if err != nil {
		logger.Error().Msg(err.Error())
		return nil, err
	}
	client := &http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		logger.Error().Msg(err.Error())
		return nil, err
	}
	if resp.StatusCode != 204 {

		body, _ := ioutil.ReadAll(resp.Body)
		// logs.Info(string(body))
		// var res interface{}
		err = json.Unmarshal(body, &response)
		return FINoTResponseDataModel{}, errors.New(string(body))
	}

	// logger.Info().Msg(string(body))
	return response, err

}

func uploadUpdatedDataToFinot(id string, token string, v []byte) (finotResponse FINoTResponseDataModel, err error) {

	var tokenPreamble string = "JWT "

	// logs.Info("The token is: " + token.Token)
	req, err := http.NewRequest("PATCH", configs.FINOTTERMINET_API+"inventory/v1/objects/"+id, bytes.NewBuffer(v))
	req.Header.Set("Content-Type", "application/json")
	req.Header.Set("X-Tenant", "terminet")
	req.Header.Set("Authorization", tokenPreamble+token)

	if err != nil {
		logger.Error().Msg(err.Error())
		return FINoTResponseDataModel{}, err
	}
	client := &http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		logger.Error().Msg(err.Error())
		return FINoTResponseDataModel{}, err
	}
	body, _ := ioutil.ReadAll(resp.Body)

	// var res interface{}
	err = json.Unmarshal(body, &finotResponse)
	if err != nil {
		logger.Error().Msg(err.Error())
		return FINoTResponseDataModel{}, errors.New(string(body))

	}

	// logs.Info(finotResponse)
	return finotResponse, nil
}
