package models

import (
	"bytes"
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	"net/http"
	"os"
	"runtime/debug"
	"terminet-iot-di-mdw/configs"
	"time"

	"github.com/astaxie/beego/cache"
	"github.com/rs/zerolog"
)

var bm cache.Cache

// set logger for package
var logger = SetLogger()

func Init() {
	var err error
	bm, err = cache.NewCache("memory", `{"interval":60}`)
	if err != nil {
		fmt.Printf("logger.Err(err): %v\n", logger.Err(err))
	}
}

func UnmarshalFinotAuthResponse(data []byte) (FinotAuthResponse, error) {
	var r FinotAuthResponse
	err := json.Unmarshal(data, &r)
	return r, err
}

func (r *FinotAuthResponse) Marshal() ([]byte, error) {
	return json.Marshal(r)
}

type FinotToken struct {
	Token string `json:"token,omitempty"`
}

type FINotCreds struct {
	LoginId string `json:"loginId,omitempty"`

	Password string `json:"password,omitempty"`
}

type FinotAuthResponse struct {
	Status int64  `json:"status"`
	Result Result `json:"result"`
}

type Result struct {
	Token        string `json:"token"`
	TenantName   string `json:"tenantName"`
	User         User   `json:"user"`
	RefreshToken string `json:"refreshToken"`
}

type User struct {
	Email               string        `json:"email"`
	ID                  string        `json:"id"`
	Firstname           string        `json:"firstname"`
	Lastname            string        `json:"lastname"`
	Tenant              string        `json:"tenant"`
	TenantTitle         string        `json:"tenantTitle"`
	Application         string        `json:"application"`
	Roles               []interface{} `json:"roles"`
	Organization        bool          `json:"organization"`
	OrganizationTenants []interface{} `json:"organizationTenants"`
}

func (fnt *FinotToken) GetFinotToken() (err error) {

	// Replace with the creds information of your choice
	// Keep in mind that storing creds in a plaintext file is not a good practice.
	// Open our jsonFile
	jsonFile, err := os.Open(configs.TENANT_CREDS_FILE)
	// if we os.Open returns an error then handle it
	if err != nil {
		logger.Error().Msg(err.Error())
		return err
	}
	// defer the closing of our jsonFile so that we can parse it later on
	byteValue, _ := ioutil.ReadAll(jsonFile)

	var creds FINotCreds
	defer jsonFile.Close()
	json.Unmarshal(byteValue, &creds)
	if bm.IsExist("finotToken") {
		fnt.Token = bm.Get("finotToken").(string)
		logger.Info().Msg("Cached Token is: " + fnt.Token)
	} else {

		// stat := map[string]string{"loginId": "finot@shapes.cy", "password": "ulNP2p7yvD5U"}
		v, _ := json.Marshal(creds)
		req, err := http.NewRequest("POST", configs.FINOTTERMINETAUT_API, bytes.NewBuffer(v))
		req.Header.Set("Content-Type", "application/json")
		req.Header.Set("X-Tenant", "terminet")

		if err != nil {
			logger.Error().Msg(err.Error())
			return err
		}
		client := &http.Client{}
		resp, err := client.Do(req)
		if err != nil {
			logger.Error().Msg(err.Error())
			// logs.Error(err)
			return err
		}
		body, _ := ioutil.ReadAll(resp.Body)
		authResp, err := UnmarshalFinotAuthResponse(body)
		if err != nil {
			logger.Error().Msg(err.Error())
			// logs.Error(err)
			return err
		}

		if authResp.Status != 200 {
			x, _ := json.Marshal(authResp)
			return errors.New("Authentication error: " + string(x))
		}

		fnt.Token = authResp.Result.Token
		bm.Put("finotToken", fnt.Token, time.Minute*50)
		defer resp.Body.Close()
	}

	return nil

}

// Set ups the logger to pretty print
func SetLogger() (logger zerolog.Logger) {
	buildInfo, _ := debug.ReadBuildInfo()
	logger = zerolog.New(zerolog.ConsoleWriter{Out: os.Stderr, TimeFormat: time.RFC3339}).
		Level(zerolog.TraceLevel).
		With().
		Timestamp().
		Caller().
		Int("pid", os.Getpid()).
		Str("go_version", buildInfo.GoVersion).
		Logger()

	return logger
}
