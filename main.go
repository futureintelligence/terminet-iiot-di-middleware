package main

import (
	docs "terminet-iot-di-mdw/docs"
	"terminet-iot-di-mdw/models"
	"terminet-iot-di-mdw/routes"

	"github.com/gin-gonic/gin"
	"github.com/rs/zerolog"
)

// @title           TERMINET I-IoT-DI MDW API
// @version         1.0
// @description     This is the API documentation for the TERMINET  Inventory  of intelligent IoT devices (I-IoT-DI) Middleware.
// // @termsOfService  http://swagger.io/terms/

// @contact.name   Argiris Sideris
// // @contact.url    http://www.swagger.io/support
// @contact.email  asideris@f-in.eu

// @license.name  EUPL 1.2
// @license.url   https://commission.europa.eu/content/european-union-public-licence_en

// @host      localhost:9997
// @BasePath  /v1

// // @securityDefinitions.basic  BasicAuth

// // @externalDocs.description  OpenAPI
// // @externalDocs.url          https://swagger.io/resources/open-api/
// set logger for package
var logger = models.SetLogger()

func main() {

	models.Init()

	zerolog.SetGlobalLevel(zerolog.InfoLevel)

	// Force log's color
	gin.ForceConsoleColor()

	r := gin.Default()

	//disable trusted proxies feature
	r.SetTrustedProxies(nil)

	// _ = docs.SwaggerInfo.Version

	// docs.SwaggerInfo.Version = "1"

	//to enable serving via gin proxy the swagger page
	docs.SwaggerInfo.Host = "localhost:3033"

	//routes
	routes.Routes(r)
	r.Run("localhost:9997")

}
