package routes

import (
	swaggerFiles "github.com/swaggo/files"
	ginSwagger "github.com/swaggo/gin-swagger"

	"terminet-iot-di-mdw/controllers"

	"github.com/gin-gonic/gin"
)

func Routes(router *gin.Engine) {
	//All routes related to the iot-di mdw come here
	v1 := router.Group("/v1")

	v1.GET("/objects/:objectid", controllers.GetInventoryObjectById)
	v1.GET("/objects", controllers.GetInventoryObjects)
	v1.POST("/objects", controllers.CreateInventoryObjects)
	v1.PATCH("/objects/:objectid", controllers.UpdateInventoryObjects)
	v1.DELETE("/objects/:objectid", controllers.DeleteInventoryObjectById)

	// set swagger endpoint
	router.GET("/swagger/*any", ginSwagger.WrapHandler(swaggerFiles.Handler))

}
